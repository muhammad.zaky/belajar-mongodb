var express = require('express');
var router = express.Router();
const studenModel = require('../models/student');

router.get('/', async function (req, res, next) {
  const student = await studenModel.find({});
  res.status(200).send(student);
});

router.post('/', async function (req, res, next) {
  const student = {
    studentName: req.body.name,
    studentNis: req.body.nis
  }

  const studentSave = new studenModel(student);
  const action = await studentSave.save();
  res.status(200).send(action);
})

router.delete('/:id', async function (req, res, next) {
  const id = req.params.id;
  const studentDelete = await studenModel.remove({ _id: id });
  res.status(200).send(studentDelete);
});

router.put('/:id', async function (req, res, next) {
  const id = req.params.id;
  const studentNewData = {
    studentName: req.body.name
  };

  const studenUpdate = await studenModel.findByIdAndUpdate(id, studentNewData);
  res.status(200).send(studenUpdate);
})

module.exports = router;
