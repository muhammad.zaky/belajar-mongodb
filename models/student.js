const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const studentSchema = new Schema({
  studentName: {
    type: String
  },
  studentNis: {
    type: Number
  }
})

module.exports = mongoose.model('students', studentSchema);